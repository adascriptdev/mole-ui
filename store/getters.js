export default {
  getScores (state) {
    return state.scores ? state.scores : []
  },
  getGlobalScores (state) {
    return state.globalScores ? state.globalScores : []
  },
  getBoardSize (state) {
    return state.board ? state.board.size : 3
  },
  getBoardState (state) {
    const monsters = {}
    if (state.board) {
      state.board.monsters.forEach(({ row, col }) => {
        monsters[`${row}-${col}`] = true
      })
    }
    return monsters
  }
}
