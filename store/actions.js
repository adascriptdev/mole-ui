export default {
  updateStatus ({ commit }, status) {
    commit('SET_STATUS', status)
  },
  setUsername ({ commit }, username) {
    commit('SET_USERNAME', username)
  }
}
