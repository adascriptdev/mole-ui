export default {
  SET_STATUS: (state, status) => {
    state.scores = status.scores
    state.board = status.board
    state.globalScores = status.globalScores
  },
  SET_USERNAME: (state, username) => {
    state.username = username
  }
}
