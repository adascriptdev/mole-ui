export default () => ({
  username: '',
  globalScores: [],
  scores: [],
  board: {
    size: 0,
    monsters: []
  }
})
