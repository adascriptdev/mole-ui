import io from 'socket.io-client'

export default ({ store }, inject) => {
  const socket = io('http://localhost:8080')

  socket.on('statusUpdate', (status) => {
    const gameStatus = JSON.parse(status)
    store.dispatch('updateStatus', gameStatus)
  })
}
